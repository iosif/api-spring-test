package fmi.gestiuneatemelor.repositories;

import fmi.gestiuneatemelor.abstr.GenericRepository;
import fmi.gestiuneatemelor.models.Person;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends GenericRepository<Person, Long> {
}
