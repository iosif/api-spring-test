package fmi.gestiuneatemelor.services;

import fmi.gestiuneatemelor.abstr.AbstractService;
import fmi.gestiuneatemelor.abstr.GenericRepository;
import fmi.gestiuneatemelor.models.Person;
import fmi.gestiuneatemelor.repositories.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class PersonService extends AbstractService<Person, Long> {
    @Autowired
    private PersonRepository repository;

    @Override
    public GenericRepository<Person, Long> getDao() {
        return repository;
    }
}
