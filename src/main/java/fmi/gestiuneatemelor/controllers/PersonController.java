package fmi.gestiuneatemelor.controllers;

import fmi.gestiuneatemelor.models.Person;
import fmi.gestiuneatemelor.services.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/persons")
@CrossOrigin
public class PersonController {
    @Autowired
    private PersonService service;

    @RequestMapping(value = "/selectAll", method = RequestMethod.GET)
    public List<Person> selectAll() {
        return service.selectAll();
    }
}
